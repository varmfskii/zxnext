#include <stdio.h>
#include <errno.h>
#include <arch/zxn/esxdos.h>
#include "zxnext.h"
#include "showsimg.h"

void readstate(void) {
  uint8_t in;
  nextstate_t new_state;

  errno=0;
  if (opts.layer==LAYER1) 
    in=esx_f_open("showsimg.l1", ESX_MODE_R);
  else
    in=esx_f_open("showsimg.l2", ESX_MODE_R);
  if (in==0xff || errno) {
    puts("read state error");
    return;
  }
  esx_f_read(in, &new_state, sizeof(nextstate_t));
  restorestate(&new_state);
  esx_f_close(in);
}

void restorestate(nextstate_t *old_state) {
  int i;
  
  if (!old_state->valid) return;
  SETNEXTREG(R_PALCTL, (opts.layer==LAYER1)?0x00:0x10);
  SETNEXTREG(R_PALIDX, 0x00);
  for(i=0; i<512; i++) SETNEXTREG(R_PALVAL9, old_state->palette[i]);
  SETNEXTREG(R_PALCTL, old_state->palctl);
  SETNEXTREG(R_ULAATTRFMT, old_state->ulaattrfmt);
  SETNEXTREG(R_GLBLTRANS, old_state->glbltrans);
  SETNEXTREG(R_CLIPCTL, 0x01);
  SETNEXTREG(R_L2CLIP, old_state->x1);
  pNextDat=old_state->x2;
  pNextDat=old_state->y1;
  pNextDat=old_state->y2;
  SETNEXTREG(R_ULACTL, old_state->ulactl);
  SETNEXTREG(R_SPRTCTL, old_state->sprtctl);
  SETNEXTREG(R_LORESCTL, old_state->loresctl);
  SETNEXTREG(R_L2CTL, old_state->l2ctl);
  SETNEXTREG(R_DISPCTL1, old_state->dispctl1);
  pTimex = old_state->timex;
  SETNEXTREG(R_PER3, old_state->per3);
  SETNEXTREG(R_MMU5, old_state->mmu5);
  SETNEXTREG(R_FBCOL, old_state->fbcol);
}

void savestate(void) {
  int i, j;
  
  state->mmu5 = GETNEXTREG(R_MMU5);
  state->per3 = GETNEXTREG(R_PER3);
  pNextDat = state->per3 | 0x04; /* enable read on timex port */
  state->timex = pTimex;
  state->dispctl1 = GETNEXTREG(R_DISPCTL1);
  state->l2ctl = GETNEXTREG(R_L2CTL);
  state->loresctl = GETNEXTREG(R_LORESCTL);
  state->sprtctl = GETNEXTREG(R_SPRTCTL);
  SETNEXTREG(R_CLIPCTL, 0x01);
  state->x1 = GETNEXTREG(R_L2CLIP);
  state->x2 = pNextDat;
  state->y1 = pNextDat;
  state->y2 = pNextDat;
  state->ulactl = GETNEXTREG(R_ULACTL);
  state->glbltrans=GETNEXTREG(R_GLBLTRANS);
  state->ulaattrfmt = GETNEXTREG(R_ULAATTRFMT);
  state->palctl = GETNEXTREG(R_PALCTL);
  state->fbcol = GETNEXTREG(R_FBCOL);
  if (opts.layer==LAYER1)
    pNextDat = 0x00;
  else
    pNextDat = 0x10;
  for(i=0, j=0; j<256; j++)  {
    SETNEXTREG(R_PALIDX, j);
    state->palette[i++] = GETNEXTREG(R_PALVAL8);
    state->palette[i++] = GETNEXTREG(R_PALVAL9);
  }
  state->valid=1;
}

void setstate(void) {
  switch(opts.mode) {
  case ULA:
    SETNEXTREG(R_SPRTCTL, 0x10);
    SETNEXTREG(R_DISPCTL1, 0x00); 
    break;
  case HICOL:
  case MLT:
  case MC:
    SETNEXTREG(R_SPRTCTL, 0x10);
    SETNEXTREG(R_DISPCTL1, 0x02); 
    break;
  case HIRES:
    SETNEXTREG(R_SPRTCTL, 0x10);
    SETNEXTREG(R_DISPCTL1, 0x06);  
    break;
  case L2_256:
  case NXI:
    SETNEXTREG(R_SPRTCTL, 0x04);
    SETNEXTREG(R_DISPCTL1, 0x80);
    SETNEXTREG(R_L2CTL, 0x00);
    SETNEXTREG(R_L2CLIP, 0);
    pNextDat=255;
    pNextDat=0;
    pNextDat=191;
    break;
  case L2_320:
    SETNEXTREG(R_SPRTCTL, 0x04);
    SETNEXTREG(R_DISPCTL1, 0x80);
    SETNEXTREG(R_L2CTL, 0x10);
    SETNEXTREG(R_CLIPCTL, 0x01);
    SETNEXTREG(R_L2CLIP, 0);
    pNextDat=159;
    pNextDat=0;
    pNextDat=255;
    break;
  case L2_640:
    SETNEXTREG(R_SPRTCTL, 0x04);
    SETNEXTREG(R_DISPCTL1, 0x80);
    SETNEXTREG(R_L2CTL, 0x20);
    SETNEXTREG(R_CLIPCTL, 0x01);
    SETNEXTREG(R_L2CLIP, 0);
    pNextDat=159;
    pNextDat=0;
    pNextDat=255;
    break;
  case LORES:
    SETNEXTREG(R_SPRTCTL, 0x90);
    SETNEXTREG(R_LORESCTL, 0x00);
    break;
  case RAD:
    SETNEXTREG(R_SPRTCTL, 0x90);
    SETNEXTREG(R_LORESCTL, 0x20);
    break;
  }
}

void writestate(void) {
  uint8_t out;
  errno=0;
  if (opts.layer==LAYER1)
    out=esx_f_open("showsimg.l1", ESX_MODE_W|ESX_MODE_OPEN_CREAT);
  else
    out=esx_f_open("showsimg.l2", ESX_MODE_W|ESX_MODE_OPEN_CREAT);
  if (out==0xff || errno) error(errno,"failed to write state",NULL);
  savestate();
  esx_f_write(out, state, sizeof(nextstate_t));
  esx_f_close(out);
}
